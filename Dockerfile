# pull official base image
FROM node:13.12.0-alpine

# Create app directory
WORKDIR /usr/src/app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
COPY package-lock.json ./

# If you are building your code for production
RUN npm install --only=production

# Bundle app source
COPY . .

RUN npm run build

EXPOSE 4000
CMD [ "npm", "run", "start"]