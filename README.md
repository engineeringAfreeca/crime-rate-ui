# Crime rate UI

## Description
The UI allow users to visualize crime information from certain uk areas. By providing the date and post code we can visualise a table and crime list information

## Task

Create an application that talks to a set of external APIs.
The solution must compile and run and written in Java.  Once complete, either zip the solution and send it back to your recruiter or share your repository link prior to your scheduled interview.

## Problem
A client wishes to build a web service and web page that allows users to enter a postcode and get crime rates in the area associated with a postcode.
The crime data should be presented in a searchable way from the web page and accessible via an API as well.

Some things to think about.
• Police data and postcode.io APIs are rate limited
• What is the best way to present the given data and is the data presented on screen the same data that should be presented through the API
• How to handle failures in API’s.

## Links
Location information for a postcode can be obtained from: [here](https://postcodes.io/)
Crime information can be obtained using the UK Police data API available at [site](https://data.police.uk/docs/method/crimes-at-location/)


### Requirements:
* CommandLine
* Docker (Optional)
* Docker-compose (Optional)

### Running the application from the command line using npm
1. Install dependencies by running the following In the base directory (containing gradle.build)
* npm install
2. running the application
* npm start

### Running just the application test

1. In the base directory (containing gradle.build)
* npm test

