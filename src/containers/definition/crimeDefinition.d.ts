type Crime = {
    category: string
    location_type: string
    context: string
    id: string
    month: string
}

type CrimeRate = {
    country: string
    longitude: number
    latitude: number
    region: string
    month: string
    crimes: Crime[]
    rate: string
}
  