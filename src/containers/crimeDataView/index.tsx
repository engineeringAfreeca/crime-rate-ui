import { retrieveCrimeRate } from "api"
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker.css'
import React, { useState } from "react"
import { Wrapper, Title, Container, LeftContainer, RightContainer, CrimeContainer, CrimeInfo, Error} from './style'
import Table from "components/table";


const CrimeDataView: React.FC = () => {
  const [crimeData, setCrimeData] =  useState<CrimeRate>();
  const [date, setDate] =  useState<Date>(new Date());
  const [postCode, setPostCode] =  useState<string>("");
  const [error, setError] =  useState<string>("");

  const getCrimeRate = () =>  {
    if(!postCode || !date){
      setError("One or more fields are empty")
      return
    }

    retrieveCrimeRate(date, postCode)
      .then(res => res.json())
      .then(response => {
        if (response.httpCode === 404) {
          setError(`Post code '${postCode}' not found`);
        } else  setCrimeData(response)
      })
      .catch(error => setError("Something went wrong"))
  }

  const inputHandler = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setError("")
    setPostCode(event.target.value);
  };

  
  const datapickerHandler = (date: Date): void => {
    setDate(date)
  }

  return (
      <Wrapper>
        <Title>Welcome to our crime rate per post-code website</Title>
        <Container>
          <LeftContainer>
            <DatePicker
              selected={date}
              onChange={(date: Date) => datapickerHandler(date)}
              dateFormat="MM/yyyy"
              minDate={new Date("01-01-2019")}
              maxDate={new Date("01-01-2022")}
              showMonthYearPicker
              startOpen
            />
            <input type="text" name="post-code" placeholder="Post Code" onChange={inputHandler}/>
            <button onClick={getCrimeRate} >Submit</button>
            { error && <Error >Error: {error}</Error>}
          </LeftContainer>
          <RightContainer>
            {!crimeData
                ?
                  <div>Loading...</div>
                :
                <CrimeContainer>
                  <CrimeInfo>
                    <h1>{crimeData.country}</h1>
                    <span>by: {crimeData.month}</span>
                    <hr />
                    <span>{crimeData.region}</span>
                  </CrimeInfo>
                  { crimeData.crimes.length !== 0 && <Table crimeList={crimeData.crimes}></Table>}
              </CrimeContainer>
            }
          </RightContainer>
        </Container>
      </Wrapper>
  )
}

export { CrimeDataView }
