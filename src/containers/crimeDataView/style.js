import styled from 'styled-components'

export const Wrapper = styled.div`
    width: 100%;   
    height: 800px;  
`;

export const Container = styled.div`
    height: 100%;
    display: grid;
    grid-gap: 5px;
    grid-template-columns: 1fr 4fr;
    align-items: center;
`;

export const LeftContainer = styled.div`
    width: 100%;   
    height: 100%; 
    display: grid;
    grid-gap: 5px;
    grid-template-rows: 20px 20px 20px 20px;
`;

export const RightContainer = styled.div`
    width: 100%;   
    height: 100%; 
    display: flex;
    justify-content: center;
    align-items: center;
    background: lightgrey;
`;

export const CrimeContainer = styled.div`
    width: 100%;   
    height: 100%; 
    display: grid;
    grid-gap: 5px;
    grid-template-rows: 200px auto;
`;


export const CrimeInfo = styled.div`
    width: 100%;   
    background: grey;
`;


export const Title = styled.p`
    text-align: center;
    font-size: 30px;
`;

export const Error = styled.span`
    color: red;
`;