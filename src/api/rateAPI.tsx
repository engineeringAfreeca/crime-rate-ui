import { convertDateToMonthYear } from "helper";


const RATE_SERVER =  process.env.REACT_APP_RATE_API;

export const retrieveCrimeRate = (date: Date, postCode: string) => {
        const dateMonth = convertDateToMonthYear(date);
        return  fetch(`${RATE_SERVER}/rate-crime/${postCode}/${dateMonth}`)
}
