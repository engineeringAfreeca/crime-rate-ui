import React from "react";
import {
  Routes,
  Route,
  Navigate,
  BrowserRouter 
} from "react-router-dom";


import Navbar from "components/navBar";
import { CrimeDataView } from "containers";

const Router = () => {  
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
          <Route path="/" element={<CrimeDataView />} />
          <Route path="*" element={<Navigate to="/" />} />  
      </Routes>
    </BrowserRouter>
  );
}

export default Router;