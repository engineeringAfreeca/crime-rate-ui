import React from 'react';
import { Container, Company, Logo } from './style'
import logo from 'assets/images/logo.svg';

const Navbar = () => {

  return (  
    <Container>
        <Company>
          <Logo src={logo}/>
        </Company> 
    </Container>
  )
}


export default Navbar;