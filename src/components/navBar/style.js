import styled from 'styled-components'

export const Container = styled.header`
    box-shadow: 0 3px 3px 5px #4d4d4d;
    width: 100%;     
    background: #fff;
    color: #fff;
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 10px;
    height: 50px
`;

export const Company = styled.div`
    display: flex;
    align-items: center;
`;

export const Logo = styled.img`
    height: 30px;
    margin-left: 25px;
    margin-right: 5px;
`;
