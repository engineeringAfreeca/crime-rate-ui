import { convertCamelCaseToString } from "helper"
import React from "react"
import { Container, Input, CustomTable, Operation, Button } from "./style"

interface Props {
  crimeList: Crime[]
}


const renderHeaders = (headers: String[]) => {
    return headers.map((name, index) => <th key={index}>{convertCamelCaseToString(name)}</th> )
}

const renderRows = (crimeList: Crime[]) => {
    return  crimeList.map(crime => (
      <tr key={crime.id}>
        <td>{crime.category}</td>
        <td>{crime.location_type}</td>
        <td>{crime.context}</td>
        <td>{crime.id}</td>
        <td>{crime.month}</td>
      </tr>  
      ))
}

const Table: React.FC <Props> = ({crimeList})=>  {
    return (
      <Container>
        <Operation>
        <Input type="text" id="myInput" placeholder="Enter search term..." title="Type in a name" />
        <Button color="green">Search</Button>
        <Button color="red">clear</Button>
        </Operation>
        <CustomTable>
            <thead>
                <tr className="header">
                    {renderHeaders(Object.keys(crimeList[0]))}
                </tr>
            </thead>
            <tbody>
                {renderRows(crimeList)}
            </tbody>
          </CustomTable>
      </Container>
    )
}

export default Table