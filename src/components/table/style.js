import styled from 'styled-components'

export const Container = styled.div`
    height:800px;
    width: 100%;
    overflow-y:auto;
`;

export const CustomTable = styled.table`
th {
    background: grey;
	position: sticky;
	position: -webkit-sticky;
    color: white;
	top: 0px;
	z-index: 2;
}
    width: 100%;
    height: 200px;
 
    th, td { 
        width: 150px; 
    }
    table, th, td {
        border: 1px solid black;
    }
    td {
        text-align: left;
        word-wrap: break-word;
    }
    button{
        &:hover {
            background-color: green;
            color: white;
            cursor: pointer;
            padding:5px
          }
    }
`;

export const Input = styled.input`
  
`;

export const Button = styled.button`
    color: ${({color}) => color || "black"};
    background-color: white;
    font-weight: bold;
`;

export const Operation = styled.div`
  display: grid;
  gap: 5px;
  grid-template-columns:200px 70px 50px;
  margin-bottom: 5px;
`;