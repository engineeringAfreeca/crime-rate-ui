export const convertCamelCaseToString = (str: String) => {
    return str.replace(/([A-Z])/g, ' $1').replace(/^./, (str) => {
      return str.toUpperCase();
    })
}