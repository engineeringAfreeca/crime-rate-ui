import { convertCamelCaseToString } from 'helper';

describe('helpers', () => {
    test('convert camel_case to uppercase string', () => {
        const input = "testId";
        const expected = "Test Id";
      expect(convertCamelCaseToString(input)).toBe(expected);
    });
})