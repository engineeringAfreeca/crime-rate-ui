export const convertDateToMonthYear = (date: Date): string => {
    const year = date.getUTCFullYear();
    const month = date.getMonth() + 1
    return `${year}-${month}`
}
