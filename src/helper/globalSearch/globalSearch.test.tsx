import { findSearchTerm } from 'helper';


describe('convert camel_case to uppercase string', () => {
    const inputA = [
        {id: 1, location_type: "Force", category: "anti-social-behaviour", context: "", month: "2020-01"},
        {id: 2, location_type: "Force", category: "Murder", context: "", month: "2020-01"}
    ]
    const searchTermA = "Murder"
    const expectedA = [{id: 2, location_type: "Force", category: "Murder", context: "", month: "2020-01"}];

    const inputB = [
        {id: 2, location_type: "Force", category: "Murder", context: "", month: "2020-01"},
        {id: 2, location_type: "Force", category: "Murder", context: "", month: "2020-01"}
    ]
    const searchTermB = "2020-01"
    const expectedB = [
        {id: 2, location_type: "Force", category: "Murder", context: "", month: "2020-01"},
        {id: 2, location_type: "Force", category: "Murder", context: "", month: "2020-01"}
    ];

    test.each`
    input       | searchTerm      | expectedResult
    ${inputA}   | ${searchTermA}  | ${expectedA}
    ${inputB}   | ${searchTermB}  | ${expectedB}
    `('find $searchTerm in $input', ({ input, searchTerm, expectedResult }) => {
        expect(findSearchTerm(input, searchTerm)).toMatchObject(expectedResult)
    })

})
