export const findSearchTerm = (crimes: Crime[], searchTerm: String) => {
    return crimes.filter(crime => {
        return crime.category.toString() === searchTerm || crime.context.includes(searchTerm.toString()) ||
        crime.location_type.includes(searchTerm.toString()) || crime.month.toString() === searchTerm

    });
}